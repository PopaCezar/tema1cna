﻿using Generated;
using Grpc.Core;
using System.Threading.Tasks;

namespace Server
{
    internal class RecieveNameService : Generated.NameRequestService.NameRequestServiceBase
    {
        public override Task<Void> ShowName(Name request, ServerCallContext context)
        { 
            System.Console.WriteLine("Hello {0}", request.Name_ + "!");
            return Task.FromResult(new Void() { });
        }
    }
}
